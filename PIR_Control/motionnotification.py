#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import os
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
from datetime import datetime

def send_mail(usr, psw, send_from, send_to, subject, text, files=[]):
    assert type(files)==list
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for f in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(f,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
        msg.attach(part)

    smtp=smtplib.SMTP('smtp.gmail.com:587')
    smtp.starttls()
    smtp.login(usr,psw)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()

if __name__ == '__main__':
     #Set the GPIO pin (board numbering) for the PIR
     PIR = 11
     pirState = False
     pirVal = False
     GPIO.setmode(GPIO.BOARD)
     GPIO.setup(PIR, GPIO.IN)
     while True:
          pirVal = GPIO.input(PIR)
          if (pirVal == True):
               #if motion is detected, check one more time to be sure
               pirVal2 = GPIO.input(PIR)
          elif GPIO.input(PIR) != True:
              time.sleep(0.5)
              if (pirVal2 ==True):
                    print "Alarm"
                    try:
                         #grab a picture from a USB webcam
                         #os.system("fswebcam /home/pi/scripts/test.jpg")
                         #grab a picture from a network attached camera, commands will vary per camera I am using an EasyN foscam clone
                         os.system("wget 'http://192.168.1.1:123/snapshot.cgi?user=user&pwd=password' -O /home/pi/motion/pirmovement.jpg")
                         # Fill these in with the appropriate info...
                         usr='gmail.user'
                         psw='password'
                         fromaddr='gmail.user@gmail.com'
                         toaddr='realEmailAddress@domain.com'
                         subject='Motion Message from RPi'
                         text='I saw something move!'
                         files=['/home/pi/motion/pirmovement.jpg']
                         #server='smtp.gmail.com:587'
                         # Send notification email
                         send_mail(usr, psw, fromaddr, toaddr, subject, text, files)
                    except Exception,msg:
                         print msg
                         #wait and do it again
                         time.sleep(25)
