from time import sleep
import piface.pfio as pfio
#import piface.emulator as pfio

pfio.init()
pfio.digital_write(1,1)
#pfio.digital_write(<output pin number>, <value>)
#piface.pfio.digital_write(1, 1)
#pfio.digital_write(4, 0)
#emulator.digital_write(3, 1)
#pfio.digital_read(<input pin number>)
led3 = pfio.LED(3)
led8 = pfio.LED(8)
led3.turn_on()

while True:
#	print bin(pfio.read_input())
# Testing if PIR movement is detected by switching leds on
	pirStatus = pfio.digital_read(5)
	if pirStatus == 1:
		print("Motion!")
		led8.turn_on()
		led3.turn_on()
		sleep(3)
		led3.turn_off()
		sleep(1)
	elif pirStatus == 0:
		led8.turn_on()