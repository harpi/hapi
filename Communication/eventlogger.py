#!/usr/bin/env python

import psycopg2
from time import sleep
import RPi.GPIO as GPIO

db="lmcc"
db_user="lmcc"
db_host="localhost"
db_password='1234567890'

polling_interval = 1
tss_table = "two_state_sensors"
tss_event = "two_state_sensor_events"

class Sensor:
    pass

class Event:
    pass

def table_exists(conn, cur, table):

    try :
        cur.execute("select * from information_schema.tables where table_name=%s", (table,))
    except Exception, e:
        print "I am have failed to check for the existence of table %s." % table
        print str(e)
        exit()

    return bool(cur.rowcount)

def create_sensor_table(conn, cur, tss_table):

    sensorTableCreationCmd = """
    CREATE TABLE %s (
        name text,
        pin_index integer,
        normal_state integer,
        normal_text text,
        abnormal_text text
    );
    """ % tss_table

    sensorTablePopulateCmd = """
    INSERT INTO %s (name, pin_index, normal_state, normal_text, abnormal_text) VALUES
    """ % tss_table

    try :
        cur.execute(sensorTableCreationCmd)
    except Exception, e:
        print "I am unable to create sensor table.", str(e)
        exit()

    for sensor_record in [ ('Front Door',       7, 0, 'closed',    'open'),
                           ('Garage Door',     11, 0, 'closed',    'open'),
                           ('Smoke Detector',  12, 1, 'quiescent', 'in alarm'),
                           ('Patio Door',      13, 0, 'closed',    'open'),
                           ('Motion Detector', 15, 0, 'quiescent', 'detects motion') ]:
        cur.execute(sensorTablePopulateCmd + " (%s, %s, %s, %s, %s)", sensor_record)

    conn.commit()

def create_event_table(conn, cur, tss_events):

    eventTableCreationCmd = """
    CREATE TABLE %s (
        description text,
        pin_index integer,
        pin_state integer,
        time_of_occurance timestamp
    );
    """ % tss_events

    cur = conn.cursor()
    try :
        cur.execute(eventTableCreationCmd)
        conn.commit()
    except Exception, e:
        print "I am unable to create event table.", str(e)
        exit()

    conn.commit()

def init_sensors(cur, sensor_table):

    try:
        cur.execute("select * from %s;" % sensor_table )
    except Exception, e:
        print "Unable to read sensor table", str(e)
        exit()

    rows = cur.fetchall()

    sensors = []

    for r in rows:
        s = Sensor()
        (s.name, s.pin_index, s.normal_state, s.normal_text, s.abnormal_text) = r
        s.state = s.normal_state
        s.previous_state = s.normal_state

        sensors.append(s)

    return sensors

def init_GPIO(sensors):

    GPIO.setmode(GPIO.BOARD)

    for s in sensors:
        GPIO.setup(s.pin_index, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def log_2state_events(conn, cur, event_table, sensors):

    insert_statement = "INSERT INTO %s " % event_table
    insert_statement += "(description, pin_index, pin_state, time_of_occurance) VALUES (%s, %s, %s, 'now');"

    for s in sensors:
        if GPIO.input(s.pin_index):
            s.state = 1
        else:
            s.state = 0

        if (s.state != s.previous_state):

            e = Event()

            if (s.state == s.normal_state):
                e.description = "%s is %s." % (s.name, s.normal_text)
            else:
                e.description = "%s is %s." % (s.name, s.abnormal_text)

            try:
                cur.execute(insert_statement, (e.description, s.pin_index, s.state ))
            except Exception, e:
                print "Unable to insert into sensor table", str(e)
                exit()

            s.previous_state = s.state

    conn.commit()

if __name__ == "__main__":

    try:
      conn = psycopg2.connect(database=db, user=db_user, host=db_host, password=db_password)
    except Exception, e:
      print "I am unable to connect to the database", str(e)
      exit()

    cur = conn.cursor()

    if (not table_exists(conn, cur, tss_table)):
        create_sensor_table(conn, cur, tss_table)
    if (not table_exists(conn, cur, tss_event)):
        create_event_table(conn, cur, tss_event)
    sensors = init_sensors(cur, tss_table)
    init_GPIO(sensors)

    while (True):
        log_2state_events(conn, cur, tss_event, sensors)
        sleep(polling_interval)

    cur.close()
    conn.close()