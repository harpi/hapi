#!/usr/bin/env python

import smtplib
from datetime import datetime

def sendEMail(usr, psw, fromaddr, toaddr):
    """
Send a test email message through GMail.
usr : the GMail username, as a string
psw : the GMail password, as a string
fromaddr : the email address the message will be from, as a string
toaddr : a email address, or a list of addresses, to send the message to
"""

    # Initialize SMTP server
    server=smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(usr,psw)

    # Send email
    senddate=datetime.strftime(datetime.now(), '%Y-%m-%d')
    subject="Test Message from RPi"
    m="Date: %s\r\nFrom: %s\r\nTo: %s\r\nSubject: %s\r\nX-Mailer: My-Mail\r\n\r\n" % (senddate, fromaddr, toaddr, subject)
    msg='''Hello all my friends. How are you today?'''

    server.sendmail(fromaddr, toaddr, m+msg)
    server.quit()


if __name__ == '__main__':

    # Fill these in with the appropriate info...
    usr='username.'
    psw='password'
    fromaddr='user.name@gmail.com'
    toaddr='destination.name@domain.com'

    # Send notification email
    sendEMail(usr, psw, fromaddr, toaddr)
