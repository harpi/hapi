#!/usr/bin/env python

from time import sleep
from datetime import datetime
import RPi.GPIO as GPIO

# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)

# set up GPIO input with pull-up control
#   (pull_up_down be PUD_OFF, PUD_UP or PUD_DOWN, default PUD_OFF)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_UP)

###################

if __name__ == '__main__':

    i=0
    prev_pin_3 = GPIO.input(3)
    prev_pin_5 = GPIO.input(5)
    prev_pin_7 = GPIO.input(7)

    prev_pin_11 = GPIO.input(11)
    prev_pin_12 = GPIO.input(12)
    prev_pin_13 = GPIO.input(13)
    prev_pin_15 = GPIO.input(15)

    while (True):
        sleep(1)

        pin_3 = GPIO.input(3)
        pin_5 = GPIO.input(5)
        pin_7 = GPIO.input(7)

        pin_11 = GPIO.input(11)
        pin_12 = GPIO.input(12)
        pin_13 = GPIO.input(13)
        pin_15 = GPIO.input(15)

        if ( ( prev_pin_3  != pin_3  ) or
             ( prev_pin_5  != pin_5  ) or
             ( prev_pin_7  != pin_7  ) or

             ( prev_pin_11 != pin_11 ) or
             ( prev_pin_12 != pin_12 ) or
             ( prev_pin_13 != pin_13 ) or
             ( prev_pin_15 != pin_15 ) ):

            print "%s - State Change %d" % (datetime.now(), i)

        if ( prev_pin_7 != pin_7  ) :
            if ( pin_7 == True ) :
                print "Front door is open."
            else:
                print "Front door is closed."

        if ( prev_pin_11 != pin_11  ) :
            if ( pin_11 == True ) :
                print "Garage door is open."
            else:
                print "Garage door is closed."

        if ( prev_pin_12 != pin_12  ) :
            if ( pin_12 == True ) :
                print "Smoke detector is quiescent."
            else:
                print "Smoke detector is active."

        if ( prev_pin_13 != pin_13 ) :
            if ( pin_13 == True ) :
                print "Patio door is open."
            else:
                print "Patio door is closed."

        if ( prev_pin_15 != pin_15 ) :
            if ( pin_15 == True ) :
                print "Motion detector active."
            else:
                print "Motion detector quiescent."


        if ( ( prev_pin_3  != pin_3  ) or
             ( prev_pin_5  != pin_5  ) or
             ( prev_pin_7  != pin_7  ) or

             ( prev_pin_11 != pin_11 ) or
             ( prev_pin_12 != pin_12 ) or
             ( prev_pin_13 != pin_13 ) or
             ( prev_pin_15 != pin_15 ) ):

            prev_pin_3 = pin_3
            prev_pin_5 = pin_5
            prev_pin_7 = pin_7

            prev_pin_11 = pin_11
            prev_pin_12 = pin_12
            prev_pin_13 = pin_13
            prev_pin_15 = pin_15

            i += 1